package git.ehcruz.demoajax.controller;

import git.ehcruz.demoajax.domain.Categoria;
import git.ehcruz.demoajax.domain.Promocao;
import git.ehcruz.demoajax.service.CategoriaService;
import git.ehcruz.demoajax.service.PromocaoDataTableService;
import git.ehcruz.demoajax.service.PromocaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/promocao")
public class PromocaoController {

    @Autowired
    private CategoriaService categoriaService;

    @Autowired
    private PromocaoService promocaoService;

    @Autowired
    private PromocaoDataTableService promocaoDataTableService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String promocaoInit() {
        return "promo-add";
    }

    @RequestMapping(value = "/salvar", method = RequestMethod.POST)
    public ResponseEntity<?> savePromocao(@Valid Promocao promocao, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            result.getFieldErrors().forEach(fieldError -> errors.put(fieldError.getField(), fieldError.getDefaultMessage()));
            return ResponseEntity.unprocessableEntity().body(errors);
        }
        this.promocaoService.salvarPromocao(promocao);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listarPromocoes(ModelMap model) {
        model.addAttribute("promocoes", this.promocaoService.listaPromocoes(0, ""));
        return "promo-list";
    }

    @RequestMapping(value = "/listar/ajax", method = RequestMethod.GET)
    public String listarPromocoesAjax(@RequestParam("page") Integer pageNumber,
                                      @RequestParam(value = "site", defaultValue = "") String site,
                                      ModelMap model) {
        model.addAttribute("promocoes", this.promocaoService.listaPromocoes(pageNumber, site));
        return "fragments/promo-card";
    }

    @RequestMapping(value = "/likes/{id}", method = RequestMethod.POST)
    public ResponseEntity<Integer> promocaoLiked(@PathVariable("id") Long idPromo) {
        this.promocaoService.addLikesOnPromocao(idPromo);
        return ResponseEntity.ok(this.promocaoService.getTotalLikesFromPromocao(idPromo));
    }

    @RequestMapping(value = "/site", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getSitesAutocomplete(@RequestParam("termo") String termo) {
        List<String> sites = this.promocaoService.getListaDeSites(termo);
        return ResponseEntity.ok(sites);
    }

    @RequestMapping(value = "/lista/site", method = RequestMethod.GET)
    public String listaPromocoesSite(@RequestParam("site") String site, ModelMap model) {
        PageRequest pageRequest = PageRequest.of(0, 4);
        model.addAttribute("promocoes", this.promocaoService.getPromocoesPorSite(site, pageRequest));
        return "fragments/promo-card";
    }

    @RequestMapping(value = "/tabela", method = RequestMethod.GET)
    public String showTable() {
        return "promo-datatables";
    }

    @RequestMapping(value = "/datatables/server", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getListaTabela(HttpServletRequest request) {
        Map<String, Object> responseJson = this.promocaoDataTableService.execute(request);
        return ResponseEntity.ok(responseJson);
    }

    @ModelAttribute("categorias")
    public List<Categoria> getCategorias() {
        return this.categoriaService.getTodasCategorias();
    }

}
