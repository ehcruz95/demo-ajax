package git.ehcruz.demoajax.controller;

import git.ehcruz.demoajax.domain.SocialMetaTag;
import git.ehcruz.demoajax.service.SocialMetaTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/meta")
public class SocialMetaTagController {

    private SocialMetaTagService socialMetaTagService;

    @Autowired
    public SocialMetaTagController(SocialMetaTagService socialMetaTagService) {
        this.socialMetaTagService = socialMetaTagService;
    }

    @RequestMapping(value = "/info", method = RequestMethod.POST)
    public ResponseEntity<SocialMetaTag> getDadosByUrl(@RequestParam("url") String url) {
        SocialMetaTag socialMetaTag = this.socialMetaTagService.getMetaTagsByUrl(url);
        return socialMetaTag != null ? ResponseEntity.ok(socialMetaTag) : ResponseEntity.notFound().build();
    }

}
