package git.ehcruz.demoajax.domain;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "TB_PROMOCAO")
public class Promocao extends AbstractDomain {

    @NotBlank(message = "{campo.obrigatorio}")
    @Column(name = "TITULO", nullable = false)
    private String titulo;

    @NotBlank(message = "{campo.obrigatorio.link}")
    @Column(name = "LINK_PROMOCAO", nullable = false)
    private String linkPromo;

    @Column(name = "SITE_PROMOCAO", nullable = false)
    private String site;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "LINK_IMAGE", nullable = false)
    private String linkImage;

    @NotNull(message = "{campo.obrigatorio.preco}")
    @DecimalMin(value = "0.00", message = "{campo.preco.valor.minimo}")
    @NumberFormat(style = NumberFormat.Style.CURRENCY, pattern = "#,##0.00")
    @Column(name = "PRECO_PROMOCAO", nullable = false)
    private BigDecimal preco;

    @Column(name = "LIKES", columnDefinition = "INTEGER DEFAULT 0")
    private Integer likes = 0;

    @Column(name = "DATA_CADASTRO", nullable = false)
    private LocalDateTime dataCadastro;

    @NotNull(message = "{campo.obrigatorio.categoria}")
    @ManyToOne
    @JoinColumn(name = "FK_CATEGORIA")
    private Categoria categoria;

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getLinkPromo() {
        return linkPromo;
    }

    public void setLinkPromo(String linkPromo) {
        this.linkPromo = linkPromo;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public LocalDateTime getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
