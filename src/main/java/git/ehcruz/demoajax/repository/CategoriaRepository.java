package git.ehcruz.demoajax.repository;

import git.ehcruz.demoajax.domain.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {
}
