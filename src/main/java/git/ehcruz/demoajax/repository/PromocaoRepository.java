package git.ehcruz.demoajax.repository;

import git.ehcruz.demoajax.domain.Promocao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Transactional(readOnly = true)
public interface PromocaoRepository extends JpaRepository<Promocao, Long> {

    @Query("SELECT COUNT(p.id) AS count, MAX(p.dataCadastro) AS lastDate FROM Promocao p WHERE p.dataCadastro > :data")
    Map<String, Object> totalAndUltimaPromocaoCadastrada(@Param("data") LocalDateTime data);

    @Query("SELECT p.dataCadastro FROM Promocao p")
    Page<LocalDateTime> findUltimaPromocaoData(Pageable pageable);

    @Query("FROM Promocao p WHERE UPPER(p.site) LIKE UPPER(CONCAT('%', :termo, '%')) " +
            "OR UPPER(p.categoria.titulo) LIKE UPPER(CONCAT('%', :termo, '%')) " +
            "OR UPPER(p.titulo) LIKE UPPER(CONCAT('%', :termo, '%'))")
    Page<Promocao> findBySearchTerm(@Param("termo") String termo, Pageable pageable);

    @Query("SELECT DISTINCT p.site FROM Promocao p WHERE UPPER(p.site) LIKE UPPER(CONCAT('%',:termo,'%'))")
    List<String> getSitesByTermo(@Param("termo") String termo);

    @Query("FROM Promocao p WHERE UPPER(p.site) LIKE UPPER(CONCAT('%',:site,'%'))")
    Page<Promocao> getPromoesBySite(@Param("site") String site, Pageable pageable);

    @Transactional(readOnly = false)
    @Modifying
    @Query("UPDATE Promocao p SET p.likes = p.likes + 1 WHERE p.id = :id")
    void updateLikesFromPromocao(@Param("id") Long id);

    @Query("SELECT p.likes FROM Promocao p WHERE p.id = :id")
    int getLikesFromPromocao(@Param("id") Long id);

}
