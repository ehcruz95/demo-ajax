package git.ehcruz.demoajax.service;

import git.ehcruz.demoajax.domain.Categoria;

import java.util.List;

public interface CategoriaService {

    List<Categoria> getTodasCategorias();
}
