package git.ehcruz.demoajax.service;

import git.ehcruz.demoajax.domain.Categoria;
import git.ehcruz.demoajax.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategoriaServiceImpl implements CategoriaService{

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Categoria> getTodasCategorias() {
        return this.categoriaRepository.findAll();
    }
}
