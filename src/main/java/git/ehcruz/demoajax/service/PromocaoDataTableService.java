package git.ehcruz.demoajax.service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface PromocaoDataTableService {

    Map<String, Object> execute(HttpServletRequest request);
}
