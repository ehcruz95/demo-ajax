package git.ehcruz.demoajax.service;

import git.ehcruz.demoajax.domain.Promocao;
import git.ehcruz.demoajax.repository.PromocaoRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
@RequestScope
public class PromocaoDataTableServiceImpl implements PromocaoDataTableService {

    @Autowired
    private PromocaoRepository promocaoRepository;

    private String[] cols = {
            "id", "titulo", "site", "linkPromo", "descricao", "linkImage", "preco", "likes", "dataCadastro", "categoria"
    };

    @Override
    public Map<String, Object> execute(HttpServletRequest request) {
        Map<String, Object> json = new HashMap<>();

        int draw = Integer.parseInt(request.getParameter("draw"));
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));

        int current = this.getCurrentPage(start, length);
        String column = this.getColumn(Integer.parseInt(request.getParameter("order[0][column]")));
        Sort.Direction direction = this.orderBy(request.getParameter("order[0][dir]"));

        String searchTerm = request.getParameter("search[value]");

        Pageable pageable = PageRequest.of(current, length, direction, column);
        Page<Promocao> promocaoPage = this.search(searchTerm, pageable);

        json.put("draw", draw);
        json.put("recordsTotal", promocaoPage.getTotalElements());
        json.put("recordsFiltered", promocaoPage.getTotalElements());
        json.put("data", promocaoPage.getContent());

        return json;
    }

    private Page<Promocao> search(String searchTerm, Pageable pageable) {
        if(StringUtils.isBlank(searchTerm)) {
            return this.promocaoRepository.findAll(pageable);
        }
        return this.promocaoRepository.findBySearchTerm(searchTerm, pageable);
    }

    private Sort.Direction orderBy(String parameter) {
        if ("ASC".equalsIgnoreCase(parameter)) {
            return Sort.Direction.ASC;
        }
        return Sort.Direction.DESC;
    }

    private String getColumn(int column) {
        return this.cols[column];
    }

    private int getCurrentPage(int start, int length) {
        return start / length;
    }
}
