package git.ehcruz.demoajax.service;

import git.ehcruz.demoajax.domain.Promocao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface PromocaoService {

    void salvarPromocao(Promocao promocao);

    Page<Promocao> listaPromocoes(Integer pageNumber, String site);

    int getTotalLikesFromPromocao(Long id);

    void addLikesOnPromocao(Long id);

    List<String> getListaDeSites(String termo);

    Page<Promocao> getPromocoesPorSite(String site, PageRequest pageRequest);
}
