package git.ehcruz.demoajax.service;

import git.ehcruz.demoajax.domain.Promocao;
import git.ehcruz.demoajax.repository.PromocaoRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class PromocaoServiceImpl implements PromocaoService {

    @Autowired
    private PromocaoRepository promocaoRepository;

    @Override
    public void salvarPromocao(Promocao promocao) {
        promocao.setDataCadastro(LocalDateTime.now());
        this.promocaoRepository.save(promocao);
    }

    @Override
    public Page<Promocao> listaPromocoes(Integer pageNumber, String site) {
        PageRequest pageRequest = PageRequest.of(pageNumber, 4, Sort.by(Sort.Direction.DESC, "dataCadastro"));
        if (StringUtils.isNotBlank(site)) {
            return this.getPromocoesPorSite(site, pageRequest);
        }
        return this.promocaoRepository.findAll(pageRequest);
    }

    @Override
    public int getTotalLikesFromPromocao(Long id) {
        return this.promocaoRepository.getLikesFromPromocao(id);
    }

    @Override
    public void addLikesOnPromocao(Long id) {
        this.promocaoRepository.updateLikesFromPromocao(id);
    }

    @Override
    public List<String> getListaDeSites(String termo) {
        return this.promocaoRepository.getSitesByTermo(termo);
    }

    @Override
    public Page<Promocao> getPromocoesPorSite(String site, PageRequest pageRequest) {
        return this.promocaoRepository.getPromoesBySite(site, pageRequest);
    }
}
