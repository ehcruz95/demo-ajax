package git.ehcruz.demoajax.service;

import git.ehcruz.demoajax.domain.SocialMetaTag;

public interface SocialMetaTagService {

    SocialMetaTag getMetaTagsByUrl(String url);

}
