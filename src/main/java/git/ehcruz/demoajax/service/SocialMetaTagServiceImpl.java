package git.ehcruz.demoajax.service;

import git.ehcruz.demoajax.domain.SocialMetaTag;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SocialMetaTagServiceImpl implements SocialMetaTagService {

    private static Logger logger = LoggerFactory.getLogger(SocialMetaTagService.class);


    @Override
    public SocialMetaTag getMetaTagsByUrl(String url) {
        SocialMetaTag tags = this.getOpenGraphByUrl(url);

        if (!this.isTagEmpty(tags)) {
            return tags;
        }

        tags = this.getTwitterCardByUrl(url);
        if (!this.isTagEmpty(tags)) {
            return tags;
        }

        return null;
    }

    private SocialMetaTag getOpenGraphByUrl(String url) {
        SocialMetaTag tag = new SocialMetaTag();

        try {
            Document doc = Jsoup.connect(url).get();
            tag.setTitle(doc.head().select("meta[property=og:title]").attr("content"));
            tag.setSite(doc.head().select("meta[property=og:site_name]").attr("content"));
            tag.setUrl(doc.head().select("meta[property=og:url]").attr("content"));
            tag.setImage(doc.head().select("meta[property=og:image]").attr("content"));
        } catch (IOException e) {
            logger.error(e.getMessage(), e.getCause());
        }

        return tag;
    }

    private SocialMetaTag getTwitterCardByUrl(String url) {
        SocialMetaTag tag = new SocialMetaTag();

        try {
            Document doc = Jsoup.connect(url).get();
            tag.setTitle(doc.head().select("meta[name=twitter:title]").attr("content"));
            tag.setSite(doc.head().select("meta[name=twitter:site]").attr("content")
                    .replace("@", ""));
            tag.setUrl(doc.head().select("meta[name=twitter:url]").attr("content"));
            tag.setImage(doc.head().select("meta[name=twitter:image]").attr("content"));
        } catch (IOException e) {
            logger.error(e.getMessage(), e.getCause());
        }

        return tag;
    }

    private boolean isTagEmpty(SocialMetaTag tag) {
        if (tag.getTitle() == null || tag.getTitle().isEmpty()) return true;
        if (tag.getSite() == null || tag.getSite().isEmpty()) return true;
        if (tag.getUrl() == null || tag.getUrl().isEmpty()) return true;
        return tag.getImage() == null || tag.getImage().isEmpty();
    }

}
