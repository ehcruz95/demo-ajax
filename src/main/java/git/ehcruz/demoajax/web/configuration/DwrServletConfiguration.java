package git.ehcruz.demoajax.web.configuration;

import org.directwebremoting.spring.DwrSpringServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@ImportResource(locations = "classpath:dwr-spring.xml")
@Configuration
public class DwrServletConfiguration {

    @Bean
    public ServletRegistrationBean<DwrSpringServlet> dwrSpringServletServletRegistrationBean() {
        DwrSpringServlet dwrSpringServlet = new DwrSpringServlet();
        ServletRegistrationBean<DwrSpringServlet> servletRegistrationBean =
                new ServletRegistrationBean<>(dwrSpringServlet, "/dwr/*");

        servletRegistrationBean.addInitParameter("debug", "true");
        servletRegistrationBean.addInitParameter("activeReverseAjax", "true");
        return servletRegistrationBean;
    }
}
