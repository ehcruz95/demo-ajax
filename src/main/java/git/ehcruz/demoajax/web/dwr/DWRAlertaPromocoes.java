package git.ehcruz.demoajax.web.dwr;

import git.ehcruz.demoajax.repository.PromocaoRepository;
import org.directwebremoting.Browser;
import org.directwebremoting.ScriptSessions;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

@Component
@RemoteProxy
public class DWRAlertaPromocoes {

    @Autowired
    private PromocaoRepository promocaoRepository;

    private Timer timer;

    @RemoteMethod
    public synchronized void init() {

        LocalDateTime lastDate = this.getUltimaDataPromocao();

        WebContext context = WebContextFactory.get();

        this.timer = new Timer();
        timer.schedule(new AlertTaks(context, lastDate), 10000, 60000);

    }

    private LocalDateTime getUltimaDataPromocao() {
        List<LocalDateTime> list = this.promocaoRepository
                .findUltimaPromocaoData(PageRequest.of(0, 1, Sort.Direction.DESC, "dataCadastro"))
                .getContent();

        if (!list.isEmpty()) {
            return list.get(0);
        }
        return LocalDateTime.now();
    }

    class AlertTaks extends TimerTask {

        private LocalDateTime lastDate;
        private WebContext context;
        private long count;

        public AlertTaks(WebContext context, LocalDateTime lastDate) {
            this.lastDate = lastDate;
            this.context = context;
        }

        @Override
        public void run() {
            Browser.withSession(context, context.getScriptSession().getId(), () -> {
                Map<String, Object> map = promocaoRepository.totalAndUltimaPromocaoCadastrada(lastDate);
                count = (Long) map.get("count");
                lastDate = map.get("lastDate") == null ? lastDate : (LocalDateTime) map.get("lastDate");

                if (count > 0) {
                    ScriptSessions.addFunctionCall("showButton", count);
                }
            });
        }
    }
}
