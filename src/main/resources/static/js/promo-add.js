'use strict';

$('#linkPromo').on('change', function () {
    var url = $(this).val();
    if (url.length > 7) {
        $.ajax('/meta/info?url=' + url, {
            method: 'POST',
            cache: false,
            success: _populateForm,
            beforeSend: function () {
                $('#loader-img').addClass('loader');
                $('#linkImagem').addClass('opacity-bg');
            },
            statusCode: {
                404: function () {
                    _errMessageRequest({
                        cssClasses: 'alert alert-danger',
                        message: 'Nenhuma informação pode ser recuperada dessa URL.'
                    })
                },
                500: function () {
                    _errMessageRequest({
                        cssClasses: 'alert alert-danger',
                        message: 'Página informada não existe.'
                    })
                }
            }
        });
    }
});

var _populateForm = function (data) {
    $('#loader-img').removeClass('loader');
    $('#titulo').val(data.title);
    $('#linkImage').removeClass('opacity-bg').attr('src', data.image);
    $('#site').text(data.site);
};

var _errMessageRequest = function (obj = {cssClasses, message}) {
    $('#loader-img').removeClass('loader');
    $('#linkImagem').removeClass('opacity-bg')
    var headAlert = $('#alert');
    headAlert.addClass(obj.cssClasses)
        .text(obj.message)
    setTimeout(function () {
        headAlert.removeClass(obj.cssClasses).text('');
    }, 2000);
    _clearForm();
};

var _clearForm = function () {
    $('#linkPromo').val('');
    $('#titulo').val('');
    $('#linkImage').attr('src', '/images/promo-dark.png');
    $('#site').text('');
    $('#categoria').val('');
    $('#preco').val('');
    $('#descricao').val('');
};

// SALVAR
$('#form-add-promo').on('submit', function (e) {
    e.preventDefault();
    var promocao = _createPromocaoObject();

    $.ajax('/promocao/salvar', {
        method: 'POST',
        data: promocao,
        cache: false,
        success: function (result) {
            $('#alert').addClass('alert alert-success').text('Promoção salva com sucesso!');
            _clearForm();
        },
        beforeSend: function () {
            $("div[id^='error-']").each(function () {
                $(this).removeClass('invalid-feedback').text('');
            });
            $('input').each(function () {
                $(this).removeClass('is-invalid');
            });
            $('select').each(function () {
                $(this).removeClass('is-invalid');
            });
            $('#form-add-promo').hide();
            $('#loader-form').addClass('loader').show();
        },
        error: function (xhr) {
            _setErrorsForm(xhr.status, xhr.responseJSON);
            _errMessageRequest({
                cssClasses: 'alert alert-danger',
                message: 'Não foi possível salvar a promoção.'
            })
        },
        complete: function () {
            $('#loader-form').fadeOut(500, function () {
                $('#form-add-promo').fadeIn(200);
                $('#loader-form').removeClass('loader');
            });
        }
    });

});

var _createPromocaoObject = function () {
    var promocao = {};
    promocao.titulo = $('#titulo').val();
    promocao.linkPromo = $('#linkPromo').val();
    promocao.site = $('#site').text();
    promocao.descricao = $('#descricao').val();
    promocao.linkImage = $('#linkImage').attr('src');
    promocao.preco = $('#preco').val();
    promocao.categoria = $('#categoria').val();
    return promocao;
};

var _setErrorsForm = function (status, errObj) {
    if (status === 422) {
        Object.keys(errObj).forEach(function (el) {
            $('#' + el).addClass('is-invalid');
            $('#error-' + el).addClass('invalid-feedback').text(errObj[el]);
        });
    }
};
