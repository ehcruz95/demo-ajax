$(document).ready(function () {
    $('#table-server').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        lengthMenu: [10, 15, 20, 25],
        ajax: {
            url: '/promocao/datatables/server',
            data: 'data'
        },
        columns: [
            {data: 'id'},
            {data: 'titulo'},
            {data: 'site'},
            {data: 'linkPromo'},
            {data: 'descricao'},
            {data: 'linkImage'},
            {data: 'preco', render: $.fn.dataTable.render.number('.', ',', 2, 'R$')},
            {data: 'likes'},
            {data: 'dataCadastro', render: $.fn.dataTable.render.dateFormat("DD/MM/YYYY")},
            {data: 'categoria.titulo'},
        ]
    });
});

$.fn.dataTable.render.dateFormat = function (format) {
    return function (data, type, row) {
        if (type === 'display') {
            return moment(data).format(format);
        }
        return data;
    };
};
