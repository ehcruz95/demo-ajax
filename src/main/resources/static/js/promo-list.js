'use strict';
var page = 0;
var lastContentLoad = 0;
var hasItEnded = false;

$(window).scroll(function () {
    var scrollTop = $(this).scrollTop();
    var conteudo = $(document).height() - $(window).height();

    if (scrollTop >= conteudo && scrollTop > lastContentLoad && !hasItEnded) {
        lastContentLoad = scrollTop;
        _loadNextPage(++page)
    }
});

var _loadNextPage = function (page) {
    var termo = $('#autocomplete-input').val();
    $.ajax('/promocao/listar/ajax', {
        method: 'GET',
        cache: false,
        data: {
            page: page,
            site: termo
        },
        success: function (result) {
            if (result.trim()) {
                $('#lista').append(result);
            } else {
                hasItEnded = true;
                $('#fim-btn').show();
            }
        },
        error: function (xhr) {
            console.log(xhr);
        },
        beforeSend: function () {
            $('#scroll-loader').show();
        },
        complete: function () {
            $('#scroll-loader').hide();
        }
    });
};

$(document).on('click', 'button[id*=\'likes-btn-\']', function () {
    var id = $(this).attr('id').split('-')[2];
    $.ajax('/promocao/likes/' + id, {
        method: 'POST',
        cache: false,
        success: function (result) {
            $('#likes-count-' + id).text(result);
        },
        error: function (xhr) {
            alert('Atenção! Ocorreu um erro ' + xhr.status + ', ' + xhr.statusText);
        }
    });
});

$('#autocomplete-input').autocomplete({
    source: function (request, response) {
        $.ajax('/promocao/site', {
            method: 'GET',
            cache: false,
            data: {
                termo: request.term
            },
            success: function (result) {
                response(result)
            },
            error: function (xhr) {
                console.log(xhr);
                response('');
            }
        });
    }
});

$('#autocomplete-submit').on('click', function () {
    var termo = $('#autocomplete-input').val();
    $.ajax('/promocao/lista/site', {
        method: 'GET',
        cache: false,
        data: {
            site: termo
        },
        beforeSend: function () {
            page = 0;
            lastContentLoad = 0;
            hasItEnded = false;
            $('.row').fadeOut(400, function () {
                $(this).empty();
            });
        },
        success: function (response) {
            $('.row').fadeIn(400, function () {
                $(this).append(response);
            });
        },
        error: function (xhr) {
            alert('Ferrou ' + xhr.status + ', ' + xhr.statusText);
        }
    });
});

//-------------

var totalOfertas = 0;

(function init() {
    console.log('Init dwr');

    dwr.engine.setActiveReverseAjax(true);
    dwr.engine.setErrorHandler(function (e) {
        console.error('DWR ERROR: ', e);
    });

    DWRAlertaPromocoes.init();
})();

function showButton(count) {
    totalOfertas += count;
    $('#btn-alert').show(function () {
        $(this)
            .attr('style', 'display:block')
            .text('Veja as novas ' + totalOfertas + ' promoções!');
    });
}
